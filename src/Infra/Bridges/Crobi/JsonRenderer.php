<?php

namespace LVC\CardsGenerator\Infra\Bridges\Crobi;

use LVC\CardsGenerator\Domain\Card;
use LVC\CardsGenerator\Domain\Generator\Output;
use LVC\CardsGenerator\Domain\Generator\RenderedCard;
use LVC\CardsGenerator\Domain\Generator\Renderer;
use LVC\CardsGenerator\Domain\Generator\Template;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * @see https://crobi.github.io/rpg-cards/generator/generate.html
 */
class JsonRenderer implements Renderer
{
    /** @var PropertyAccessorInterface */
    private $propertyAccessor;

    public function __construct(PropertyAccessorInterface $propertyAccessor)
    {
        $this->propertyAccessor = $propertyAccessor;
    }

    public function renderCard(Template $template, Card $card): RenderedCard
    {
        return new JsonRenderedCard(
            [
                'count' => $card->getQuantity(),
                'color' => $card->getBackgroundColor(),
                'title' => $card->getName(),
                'icon' => $card->getIcon(),
                'icon_back' => $card->getBackIcon(),
                'contents' => $this->getContent($template, $card),
                'tags' => [],
            ]
        );
    }

    private function getContent(Template $template, Card $card): array
    {
        return array_map(
            function ($contentLine) use ($card): string {
                return preg_replace_callback(
                    '#{(?<accessor>[-.\w\[\]]+)}#',
                    function (array $matches) use ($card) {
                        try {
                            return $this->propertyAccessor->getValue($card, $matches['accessor']);
                        } catch (\Exception $e) {
                            return '';
                        }
                    },
                    $contentLine
                );
            },
            $template->getContentLines()
        );
    }

    public function formatOutputsToJson(Output ...$outputs): string
    {
        $renderedCards = [];

        foreach ($outputs as $output) {
            $renderedCards = array_merge(
                $renderedCards,
                array_map(
                    function (RenderedCard $renderedCard) {
                        return $renderedCard->toString();
                    },
                    $output->getRenderedCards()
                )
            );
        }

        return '['.implode(',', $renderedCards).']';
    }
}
