<?php

namespace LVC\CardsGenerator\Domain\Generator;

use LVC\CardsGenerator\Domain\Card;

class Generator
{
    public function generate(Renderer $renderer, Template $template, Card ...$cards): Output
    {
        $output = new Output();

        foreach ($cards as $card) {
            $output->addRenderedCard($renderer->renderCard($template, $card));
        }

        return $output;
    }
}
