<?php

namespace LVC\CardsGenerator\Domain;

class Card
{
    /** @var string */
    private $name;
    /** @var int */
    private $quantity;
    /** @var string|null */
    private $backgroundColor;
    /** @var string|null */
    private $icon;
    /** @var string|null */
    private $backIcon;
    /** @var string|null */
    private $rule;
    /** @var string|null */
    private $description;
    /** @var array[] */
    private $properties;

    public function __construct(
        string $name,
        int $quantity,
        ?string $backgroundColor,
        ?string $icon,
        ?string $backIcon,
        ?string $rule,
        ?string $description,
        array $properties
    ) {
        $this->name = $name;
        $this->quantity = $quantity;
        $this->backgroundColor = $backgroundColor;
        $this->icon = $icon;
        $this->backIcon = $backIcon;
        $this->rule = $rule;
        $this->description = $description;
        $this->properties = $properties;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getBackgroundColor(): ?string
    {
        return $this->backgroundColor;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function getBackIcon(): ?string
    {
        return $this->backIcon;
    }

    public function getRule(): ?string
    {
        return $this->rule;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getProperties(): array
    {
        return $this->properties;
    }
}
