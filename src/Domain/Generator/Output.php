<?php

namespace LVC\CardsGenerator\Domain\Generator;

class Output
{
    /** @var RenderedCard[] */
    private $renderedCards;

    public function addRenderedCard(RenderedCard $renderedCard)
    {
        $this->renderedCards[] = $renderedCard;
    }

    /** @return RenderedCard[] */
    public function getRenderedCards(): array
    {
        return $this->renderedCards;
    }
}
