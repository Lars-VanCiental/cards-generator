<?php

namespace LVC\CardsGenerator\Domain\Generator;

use LVC\CardsGenerator\Domain\Card;

interface Renderer
{
    public function renderCard(Template $template, Card $card): RenderedCard;
}
