<?php

namespace LVC\CardsGenerator\Domain\Generator;

class Template
{
    /** @var string */
    private $name;
    /** @var string[] */
    private $contentLines;

    public function __construct(string $name, string ...$contentLines)
    {
        $this->name = $name;
        $this->contentLines = $contentLines;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getContentLines(): array
    {
        return $this->contentLines;
    }
}
