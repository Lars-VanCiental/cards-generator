<?php

require __DIR__.'/../vendor/autoload.php';

use LVC\CardsGenerator\Infra\Command\CardsGeneratorCommand;
use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new CardsGeneratorCommand());

$application->run();
