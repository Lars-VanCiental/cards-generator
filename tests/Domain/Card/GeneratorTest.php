<?php

namespace LVC\CardsGeneratorTests\Domain\Card;

use LVC\CardsGenerator\Domain\Card;
use LVC\CardsGenerator\Domain\Generator\Generator;
use LVC\CardsGenerator\Domain\Generator\RenderedCard;
use LVC\CardsGenerator\Domain\Generator\Renderer;
use LVC\CardsGenerator\Domain\Generator\Template;
use PHPUnit\Framework\TestCase;

class GeneratorTest extends TestCase
{
    public function test_generate(): void
    {
        $templateMock = $this->createMock(Template::class);

        $card1Mock = $this->createMock(Card::class);
        $card2Mock = $this->createMock(Card::class);

        $renderedCard1Mock = $this->createMock(RenderedCard::class);
        $renderedCard2Mock = $this->createMock(RenderedCard::class);

        $rendererMock = $this->createMock(Renderer::class);
        $rendererMock
            ->expects($this->exactly(2))
            ->method('renderCard')
            ->withConsecutive(
                [$templateMock, $card1Mock],
                [$templateMock, $card2Mock]
            )->willReturnOnConsecutiveCalls(
                $renderedCard1Mock,
                $renderedCard2Mock
            );

        $generator = new Generator();

        $output = $generator->generate($rendererMock, $templateMock, $card1Mock, $card2Mock);

        $this->assertSame([$renderedCard1Mock, $renderedCard2Mock], $output->getRenderedCards());
    }
}
