<?php

namespace LVC\CardsGenerator\Domain\Generator;

interface RenderedCard
{
    public function toString(): string;
}
