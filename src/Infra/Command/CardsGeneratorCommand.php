<?php

namespace LVC\CardsGenerator\Infra\Command;

use LVC\CardsGenerator\Domain\Card;
use LVC\CardsGenerator\Domain\Generator\Generator;
use LVC\CardsGenerator\Domain\Generator\Renderer;
use LVC\CardsGenerator\Domain\Generator\Template;
use LVC\CardsGenerator\Infra\Bridges\Crobi\JsonRenderer;
use LVC\CardsGenerator\Infra\Card\CardsLoader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CardsGeneratorCommand extends Command
{
    const ARGUMENT_INPUT_PATH = 'input-path';
    const OPTION_OUTPUT_PATH = 'output-path';
    const OPTION_OUTPUT_FILENAME = 'output-filename';

    protected function configure()
    {
        $this->setName('cards:generate');
        $this->addArgument(
            self::ARGUMENT_INPUT_PATH,
            InputArgument::REQUIRED,
            'Path to the project s cards configurations folder.'
        );
        $this->addOption(
            self::OPTION_OUTPUT_PATH,
            null,
            InputOption::VALUE_REQUIRED,
            'Path to the project s cards result folder, by default, same as the input.'
        );
        $this->addOption(
            self::OPTION_OUTPUT_FILENAME,
            null,
            InputOption::VALUE_REQUIRED,
            'Filename of the project s cards result file.',
            'result'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $projectInputFolder = $this->getProjectInputFolder($input);
        $projectOutputFolder = $this->getProjectOutputFolder($input, $projectInputFolder);
        $projectOutputFilename = $this->getProjectOutputFilename($input);

        $projectFiles = glob($projectInputFolder.'*.yaml');
        if ($projectFiles === false) {
            throw new \InvalidArgumentException('Could not load project files.');
        }

        $io->title('starting generation');
        $io->text('Using '.count($projectFiles).' configurations files found in "'.$projectInputFolder.'".');

        /** @var JsonRenderer $renderer */
        $renderer = $this->getRenderer();
        $cardsGenerator = new Generator();

        $cardsCount = 0;
        $outputs = [];
        foreach ($this->getCards($projectFiles) as $cardTypeName => $cardType) {
            /** @var Template $template */
            /** @var Card[] $cards */
            [$template, $cards] = $cardType;
            $cardsCount += count($cards);
            $outputs[] = $cardsGenerator->generate($renderer, $template, ...$cards);
        }

        $io->text(count($outputs).' types of cards detected.');

        file_put_contents($filename = $projectOutputFolder.$projectOutputFilename.'.json', $renderer->formatOutputsToJson(...$outputs));

        $io->success($cardsCount.' cards rendered in "'.$filename.'".');
    }

    private function getProjectInputFolder(InputInterface $input): string
    {
        $projectInputFolder = $input->getArgument(self::ARGUMENT_INPUT_PATH);
        if (!is_string($projectInputFolder) || !is_dir($projectInputFolder)) {
            throw new \InvalidArgumentException('Given project input path is not a valid folder.');
        }
        if (substr($projectInputFolder, -1) !== DIRECTORY_SEPARATOR) {
            $projectInputFolder .= DIRECTORY_SEPARATOR;
        }

        return $projectInputFolder;
    }

    private function getProjectOutputFolder(InputInterface $input, string $defaultValue): string
    {
        $projectOutputFolder = $input->getOption(self::OPTION_OUTPUT_PATH);
        if (empty($projectOutputFolder)) {
            $projectOutputFolder = $defaultValue;
        }
        if (!is_string($projectOutputFolder) || !is_dir($projectOutputFolder)) {
            throw new \InvalidArgumentException('Given project output path is not a valid folder.');
        }
        if (substr($projectOutputFolder, -1) !== DIRECTORY_SEPARATOR) {
            $projectOutputFolder .= DIRECTORY_SEPARATOR;
        }

        return $projectOutputFolder;
    }

    private function getProjectOutputFilename(InputInterface $input): string
    {
        $projectOutputFilename = $input->getOption(self::OPTION_OUTPUT_FILENAME);
        if (!is_string($projectOutputFilename) || empty($projectOutputFilename)) {
            throw new \InvalidArgumentException('Given project output filename is not a valid filename.');
        }

        return $projectOutputFilename;
    }

    private function getRenderer(): Renderer
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->disableExceptionOnInvalidIndex()
            ->disableExceptionOnInvalidPropertyPath()
            ->getPropertyAccessor();

        return new JsonRenderer($propertyAccessor);
    }

    private function getCards(array $projectFiles): \Generator
    {
        return (new CardsLoader())->getCards(...$projectFiles);
    }
}
