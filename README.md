# cards-generator #

Generating cards from yaml for various tools with different pre-defined templates.

## Usage ##

There is one command available to use.

### Crobi cards generator ###

It will generate a single json file for Crobi cards generator (see https://crobi.github.io/rpg-cards/generator/generate.html).

```shell
$ php bin/application.php cards:generate assets/example/ --output-path assets/result --output-filename example
```

### Configuration

[See the exemples files](assets/example/examples.yaml)
