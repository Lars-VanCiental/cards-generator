<?php

namespace LVC\CardsGenerator\Infra\Card;

use LVC\CardsGenerator\Domain\Card;
use LVC\CardsGenerator\Domain\Generator\Template;
use Symfony\Component\Yaml\Yaml;

class CardsLoader
{
    /** @return \Generator Returns an array of arrays containing a Renderer and a list of Card */
    public function getCards(string ...$files): \Generator
    {
        foreach ($files as $file) {
            if (!file_exists($file)) {
                throw new \InvalidArgumentException('The file "'.$file.'" does not exist.');
            }

            yield from $this->parseFile($file);
        }
    }

    private function parseFile(string $file): \Generator
    {
        $fileContent = Yaml::parseFile($file);

        foreach ($fileContent as $cardTypeName => $cardType) {
            yield $cardTypeName => $this->buildCards($cardTypeName, $cardType);
        }
    }

    private function buildCards(string $cardTypeName, array $cardType): array
    {
        $template = new Template($cardTypeName, ...((array) ($cardType['template'] ?? [])));

        $commonAttributes = $cardType['common'] ?? [];

        $cards = [];
        foreach ($cardType['cards'] ?? [] as $cardName => $cardAttributes) {
            $cards[] = self::createCard($cardName, $commonAttributes, $cardAttributes);
        }

        return [$template, $cards];
    }

    private static function createCard(string $cardName, array $commonAttributes, array $cardAttributes): Card
    {
        $data = array_merge($commonAttributes, $cardAttributes);
        $multiplier = (int) ($data['multiplier'] ?? 1);

        return new Card(
            $cardName,
            $multiplier * (int) ($data['quantity'] ?? 1),
            ($data['background-color'] ?? null) !== null ? (string) $data['background-color'] : null,
            ($data['icon'] ?? null) !== null ? (string) $data['icon'] : null,
            ($data['back-icon'] ?? null) !== null ? (string) $data['back-icon'] : null,
            ($data['rule'] ?? null) !== null ? (string) $data['rule'] : null,
            ($data['description'] ?? null) !== null ? (string) $data['description'] : null,
           $data['properties'] ?? []
        );
    }
}
