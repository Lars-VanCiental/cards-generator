<?php

namespace LVC\CardsGenerator\Infra\Bridges\Crobi;

use LVC\CardsGenerator\Domain\Generator\RenderedCard;

class JsonRenderedCard implements RenderedCard
{
    /** @var string */
    private $json;

    public function __construct(array $data)
    {
        if (($json = json_encode($data)) === false) {
            throw new \InvalidArgumentException('Could not json encode the card data.');
        }

        $this->json = $json;
    }

    public function toString(): string
    {
        return $this->json;
    }
}
